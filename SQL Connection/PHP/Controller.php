<?php
	include_once 'Models/Pais.php';
	include_once "SQL/InsertSQL.php";
	include_once "SQL/SelectSQL.php";
	include_once "SQL/UpdateSQL.php";	
	include_once "SQL/DeleteSQL.php";

	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	header('Content-Type: application/json');
	
	try {
		// Insert();
		// Select();
		// Update();
		// Delete();
	} catch (Exception $e) {
		$return = array(
			'resultado' => $e->getMessage()
		);
		http_response_code(501);
		echo(json_encode($return));	
	}

	function Insert() {
		$venezuela = new Pais('Venezuela', 'America');
		$colombia = new Pais('Kolonvia', 'America');
		$alemania = new Pais('Alemania', 'Europa');
		$francia = new Pais('Francia', 'Europa');
	
		$insert = new InsertSQL();
		var_dump($insert->Insertar($venezuela));
		var_dump($insert->Insertar($colombia));
		var_dump($insert->Insertar($alemania));
		var_dump($insert->Insertar($francia));
	}	

	function Select() {
		$condiciones = array(
			array("Continente" => array("Valor" => "Europa", "Condicion" => "=" ))
		);
		$select = new SelectSQL();
		$result = $select->Obtener(new Pais(), $condiciones);
		var_dump($result);
	}

	function Update(){
		$condiciones = array(
			array("Pais_Id" => array("Valor" => "2", "Condicion" => "=" ))
		);
		$colombia = new Pais('Colombia', 'America');
		$update = new UpdateSQL();
		$update->Actualizar($colombia, $condiciones);
	}

	function Delete(){
		$condiciones = array(
			array("Continente" => array("Valor" => "America", "Condicion" => "=" ))
		);
		$delete = new DeleteSQL();
		$delete->Eliminar(new Pais(), $condiciones);
	}
?>