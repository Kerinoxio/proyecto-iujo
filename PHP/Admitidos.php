<?php
include_once ('Estudiante.php');
include_once ('ConsignedDocuments.php');
include_once ('recibirDatos.php');
include ('conectardb.php');
//include_once ('Conexion.php');

$documentos = new Documentos();
$documentos->DocumentosSolicitados ($copia_cedula, $copia_fecha_nacimiento, $copia_titulo, $copia_notas, $copia_opsu, $copia_fondo_negro);
$estudiante = new Estudiante();
$estudiante->DatosPersonales ($cedula, $nombre, $apellido, $fecha_nacimiento, $genero);
//$con = new Conexion($con);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="editar.php" method="post">
	<center>
	<fieldset>
		<legend>Lista de Estudiantes Ingresados</legend>
		<table>
			<thead>
				<tr>
					<th>Cédula</th>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Fecha de nacimiento</th>
					<th>Género</th>
					<th>C.Cédula</th>
					<th>Part. nac.</th>
					<th>Título</th>
					<th>Notas Cert.</th>
					<th>OPSU</th>
					<th>Fondo Negro</th>
				</tr>
			</thead>
			<tbody>
			<?php  while ($fila = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
				//printf("ID: %s  Nombre: %s Apellido: %s", $fila["id"], $fila["nombre"], $fila["apellido"]);
				echo "<br>";
    		 ?>
				<tr>
					<td><?php  printf("%s" , $fila["cedula"]);?></td>
				 	<td><?php  printf("%s" , $fila["nombre"]);?></td>
					<td><?php  printf("%s" , $fila["apellido"]);?></td>
					<td><?php  printf("%s" , $fila["fechaNacimiento"]);?></td>
					<td><?php  printf("%s" , $fila["genero"]);?></td>
					<td><?php  printf("%s" , $fila["copiaCedula"]);?></td>
					<td><?php  printf("%s" , $fila["copiaPartidaNacimiento"]);?></td>
					<td><?php  printf("%s" , $fila["copiaTituloBachiller"]);?></td>
					<td><?php  printf("%s" , $fila["copiaNotasCertificadas"]);?></td>
					<td><?php  printf("%s" , $fila["copiaOpsu"]);?></td>
					<td><?php  printf("%s" , $fila["fondoNegro"]);?>
					<td><input type="submit" value="Editar"></td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</fieldset>
	<input type="submit" name="Volver al inicio" value="Regresar a la página principal">
	</form>
	</center>
</body>
</html>